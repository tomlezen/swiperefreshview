package com.tlz.refresh

import android.content.Context
import android.util.TypedValue

/**
 *
 * Created by Tomlezen.
 * Date: 2017/9/13.
 * Time: 14:12.
 */
internal fun Context.dp2px(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
package com.tlz.refresh.def

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.view.animation.LinearInterpolator
import com.tlz.refresh.dp2px

/**
 * Created by LeiShao
 * Date: 2017/1/21
 * Time: 16:24
 * Email: 1335686009@qq.com
 */
class ThreeLineProgressDrawable(private val ctx: Context) : Drawable(), Animatable {

  private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
    style = Paint.Style.STROKE
    strokeWidth = ctx.dp2px(2f)
    strokeCap = Paint.Cap.ROUND
    strokeJoin = Paint.Join.ROUND
  }

  private val rectFs = arrayOf(RectF(), RectF(), RectF())

  private val colors = intArrayOf(Color.parseColor("#ffb709"), Color.parseColor("#66cad0"), Color.parseColor("#ff75d6"))

  private val rotateSpeed = floatArrayOf(2f, 6f, 10f)

  private val rotateAngle = floatArrayOf(0f, 120f, 240f)

  private var size = ctx.dp2px(40f)
  private var lineSpace = ctx.dp2px(4f)

  private var animator: ValueAnimator? = null

  private var angle: Float = 0.toFloat()

  private var isRunningFinishProgress = false

  private var centerX = 0f
    get() = bounds.centerX().toFloat()
  private var centerY = 0f
    get() = bounds.centerY().toFloat()

  override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
    super.setBounds(left, top, right, bottom)
    val padding = lineSpace
    for (i in 0..2) {
      rectFs[2 - i].set(padding + lineSpace * i, padding + lineSpace * i, size - (lineSpace * i) - padding, size - (lineSpace * i) - padding)
    }
  }

  fun setSwipeProgress(progress: Float) {
    if (isRunning)
      return
    if(progress == 0f){
      isRunningFinishProgress = false
    }
    this.angle = (if(isRunningFinishProgress) (1 - progress) else progress) * 360
    invalidateSelf()
  }

  override fun start() {
    if (isRunning) {
      return
    }
    setSwipeProgress(0f)
    (0 until rotateAngle.size).forEach { rotateAngle[it] = it * 120f }
    animator = ValueAnimator.ofFloat(0f, 1f).apply {
      interpolator = LinearInterpolator()
      duration = 1000
      repeatCount = -1
      addUpdateListener {
        (0 until rotateAngle.size).forEach {
          rotateAngle[it] = (rotateAngle[it] + rotateSpeed[it]) % 360
        }
        invalidateSelf()
      }
    }
    animator?.start()
  }

  override fun stop() {
    animator?.let {
      if(isRunning){
        it.end()
        isRunningFinishProgress = true
      }
    }
  }

  override fun isRunning(): Boolean = animator?.isRunning ?: false

  override fun draw(canvas: Canvas) {
    if (angle > 0f) {
      canvas.save()
      if (angle > 360)
        canvas.rotate(angle - 360, centerX, centerY)
      if(isRunningFinishProgress){
        for (i in 0..2) {
          paint.color = colors[i]
          if (angle > 120 * i && angle < (120 * i + 90)) {
            canvas.drawArc(rectFs[i], rotateAngle[i] + (angle - 120 * i),   90 - (angle - 120 * i), false, paint)
          }else if (angle <= 120 * i) {
            canvas.drawArc(rectFs[i], rotateAngle[i], 90f, false, paint)
          }
        }
      }else{
        for (i in 0..2) {
          paint.color = colors[i]
          if (angle > (i + 1) * 90 + i * 30) {
            canvas.drawArc(rectFs[i], (i * 120).toFloat(), 90f, false, paint)
          }else if (angle > i * 120) {
            canvas.drawArc(rectFs[i], (i * 120).toFloat(), angle - 120 * i, false, paint)
          }
        }
      }
      canvas.restore()
    } else if (isRunning) {
      for (i in 0..2) {
        canvas.save()
        canvas.rotate(rotateAngle[i], centerX, centerY)
        paint.color = colors[i]
        canvas.drawArc(rectFs[i], 0f, 90f, false, paint)
        canvas.restore()
      }
    }
  }

  override fun setAlpha(alpha: Int) {}

  override fun setColorFilter(colorFilter: ColorFilter?) {}

  override fun getOpacity(): Int = 0

  override fun getIntrinsicHeight(): Int = size.toInt()

  override fun getIntrinsicWidth(): Int = size.toInt()

  fun setProgressColorSchemeColors(inColor: Int, midColor: Int, outColor: Int){
    colors[0] = inColor
    colors[1] = midColor
    colors[2] = outColor
    if(!isRunning){
      invalidateSelf()
    }
  }
}

package com.tlz.refresh.def

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.ImageView
import com.tlz.refresh.RefreshActionView
import com.tlz.refresh.SwipeRefreshView.SwipeDirect
import com.tlz.refresh.dp2px

/**
 *
 * Created by Tomlezen.
 * Date: 2017/9/12.
 * Time: 15:59.
 */
class DefRefreshView(context: Context, attrs: AttributeSet? = null): FrameLayout(context, attrs), RefreshActionView {

  private val progressDrawable by lazy { ThreeLineProgressDrawable(context) }

  init {
    val padding = context.dp2px(10f).toInt()
    setPadding(padding, padding, padding, padding)
    addView(ImageView(context).apply {
      setImageDrawable(progressDrawable)
    }, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply { gravity = Gravity.CENTER })
  }

  override fun getSwipeMaxDist(): Int = height

  override fun onSwipe(direct: SwipeDirect, percent: Float) {
    progressDrawable.setSwipeProgress(percent)
  }

  override fun onRefreshStart() {
    progressDrawable.start()
  }

  override fun onRefreshFinish() {
    progressDrawable.stop()
  }

  fun setProgressColorSchemeColors(inColor: Int, midColor: Int, outColor: Int){
    progressDrawable.setProgressColorSchemeColors(inColor, midColor, outColor)
  }

}
package com.tlz.refresh

/**
 * Created by Tomlezen.
 * Date: 2017/8/15.
 * Time: 上午9:34.
 */
interface RefreshActionView{

  fun getSwipeMaxDist(): Int

  fun onSwipe(direct: SwipeRefreshView.SwipeDirect, percent: Float)

  fun onRefreshStart()

  fun onRefreshFinish()

}
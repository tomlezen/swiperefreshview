package com.tlz.refresh

/**
 * Created by Tomlezen.
 * Date: 2017/8/15.
 * Time: 上午9:03.
 */
interface OnRefreshListener {
  fun onRefreshTop()
  fun onRefreshBot()
}
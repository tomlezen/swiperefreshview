package com.tlz.refresh

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.view.NestedScrollingChild
import android.support.v4.view.NestedScrollingChildHelper
import android.support.v4.view.NestedScrollingParent
import android.support.v4.view.NestedScrollingParentHelper
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.DecelerateInterpolator
import android.view.animation.Transformation
import android.widget.AbsListView
import com.tlz.refresh.SwipeRefreshView.SwipeDirect.BOT
import com.tlz.refresh.SwipeRefreshView.SwipeDirect.IDLE
import com.tlz.refresh.SwipeRefreshView.SwipeDirect.TOP
import com.tlz.refresh.def.DefRefreshView

/**
 * Created by Tomlezen.
 * Date: 2017/8/14.
 * Time: 下午6:44.
 */
class SwipeRefreshView(context: Context, attrs: AttributeSet) : ViewGroup(context, attrs), NestedScrollingParent, NestedScrollingChild {

  private var target: View? = null
  private var topRefreshView: RefreshActionView = DefRefreshView(context)
  private var botRefreshView: RefreshActionView = DefRefreshView(context)

  var swipeMode = Mode.TOP
    set(value) {
      if (isInitFinish && value != field) {
        refreshFinish()
      }
      field = value
    }
  private var offsetAnimationDuration: Int
  private var touchSlop = ViewConfiguration.get(context).scaledTouchSlop
  private var dragRate: Float
  private var totalDragDis = 0
  private var dragPercent = 0f
  private var offsetTop = 0
  private var from = 0
  private var fromDragPercent = 0f
  private var isRefreshing = false
  private var isBeingDragged = false
  private var isInitFinish = false
  private var isReturnStart = false
  private var notify = false

  private var targetPaddingLeft = 0
  private var targetPaddingRight = 0
  private var targetPaddingTop = 0
  private var targetPaddingBottom = 0

  private var onRefreshListener: OnRefreshListener? = null
  private var swipeDirect = SwipeDirect.IDLE

  private val nestedScrollingParentHelper = NestedScrollingParentHelper(this)
  private val nestedScrollingChildHelper = NestedScrollingChildHelper(this)
  private var totalUnconsumed = 0f
  private var nestedScrollInProgress = false
  private val parentScrollConsumed = IntArray(2)
  private val parentOffsetInWindow = IntArray(2)

  private var animateToStartPosition = object : Animation() {

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
      moveToStart(interpolatedTime)
    }

  }

  private var animateToCorrectPosition = object : Animation() {

    private var direct = SwipeDirect.IDLE

    override fun setStartTime(startTimeMillis: Long) {
      this.direct = swipeDirect
      super.setStartTime(startTimeMillis)
    }

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
      var targetTop = 0
      val endTarget = totalDragDis
      if (direct == TOP) {
        targetTop = (from + (endTarget - from) * interpolatedTime).toInt()
        dragPercent = fromDragPercent - (fromDragPercent - 1f) * interpolatedTime
        topRefreshView.onSwipe(direct, dragPercent)
      } else if (direct == BOT) {
        targetTop = (from - (endTarget + from) * interpolatedTime).toInt()
        dragPercent = fromDragPercent - (fromDragPercent + 1f) * interpolatedTime
        botRefreshView.onSwipe(direct, Math.abs(dragPercent))
      }
      val offset = targetTop - (target?.top ?: 0)
      setTargetOffset(offset)
    }
  }

  private var initMotionY = 0f
  private var activePointerId = -1

  var refreshEnable = true
    set(value) {
      if (!value && isInitFinish) {
        refreshFinish()
      }
      field = value
    }

  init {
    val ta = context.obtainStyledAttributes(attrs, R.styleable.SwipeRefreshView)
    swipeMode = Mode.get(ta.getInt(R.styleable.SwipeRefreshView_mode, Mode.TOP.value))
    dragRate = ta.getFloat(R.styleable.SwipeRefreshView_dragRate, DEF_DRAG_RATE)
    offsetAnimationDuration = ta.getInt(R.styleable.SwipeRefreshView_offsetAnimDuration, DEF_OFFSET_ANIMATION_DURATION)
    ta.recycle()

    ensureTarget()
    setWillNotDraw(false)
    ViewCompat.setChildrenDrawingOrderEnabled(this, true)
    isInitFinish = true
  }

  override fun onLayout(p0: Boolean, p1: Int, p2: Int, p3: Int, p4: Int) {
    ensureTarget()
    if (target == null)
      return

    val height = measuredHeight
    val width = measuredWidth
    val left = paddingLeft
    val top = paddingTop
    val right = paddingRight
    val bottom = paddingBottom

    target?.layout(left, top + offsetTop, width - right, top + height - bottom + offsetTop)
    (0 until childCount).map { getChildAt(it) }.filter { it is RefreshActionView }.forEach {
      if (it == topRefreshView) {
        it.layout(left, top, left + width - right, top + it.measuredHeight)
      } else if (it == botRefreshView) {
        it.layout(left, height - bottom - it.measuredHeight, left + width - right, height - bottom)
      }
    }
  }

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    ensureTarget()
    if (target == null) {
      return
    }

    val width = MeasureSpec.makeMeasureSpec(measuredWidth - paddingLeft - paddingRight, MeasureSpec.EXACTLY)
    val height = MeasureSpec.makeMeasureSpec(measuredHeight - paddingTop - paddingBottom, MeasureSpec.EXACTLY)
    target?.measure(width, height)
    (0 until childCount).map { getChildAt(it) }.filter { it is RefreshActionView }.forEach {
      val lp = it.layoutParams

      val childWidthMeasureSpec = if (lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {
        View.MeasureSpec.makeMeasureSpec(Math.max(0, measuredWidth - paddingLeft - paddingRight), View.MeasureSpec.EXACTLY)
      } else {
        ViewGroup.getChildMeasureSpec(widthMeasureSpec, paddingLeft + paddingRight, lp.width)
      }

      val childHeightMeasureSpec = if (lp.height == ViewGroup.LayoutParams.MATCH_PARENT) {
        View.MeasureSpec.makeMeasureSpec(Math.max(0, measuredHeight - paddingTop - paddingBottom), View.MeasureSpec.EXACTLY)
      } else {
        ViewGroup.getChildMeasureSpec(heightMeasureSpec, paddingTop + paddingBottom, lp.height)
      }

      it.measure(childWidthMeasureSpec, childHeightMeasureSpec)
    }
  }

  override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
    if (isReturnStart && ev?.action == MotionEvent.ACTION_DOWN) {
      isReturnStart = false
    }

    if (!isEnabled || !refreshEnable || isRefreshing || isReturnStart || !canScroll() || nestedScrollInProgress) {
      return false
    }

    when (ev?.action) {
      MotionEvent.ACTION_DOWN -> {
        setTargetOffset(0)
        activePointerId = ev.getPointerId(0)
        isBeingDragged = false
        val initMotionY = getMotionEventY(ev, activePointerId)
        if (initMotionY < 0) {
          return false
        }
        this@SwipeRefreshView.initMotionY = initMotionY
      }
      MotionEvent.ACTION_MOVE -> {
        if (activePointerId == -1) {
          return false
        }
        val y = getMotionEventY(ev, activePointerId)
        if (y < 0) {
          return false
        }
        val yDiff = y - initMotionY
        if (!isBeingDragged && ((yDiff > 0 && swipeMode.value and Mode.TOP.value != 0 && !canScrollDown()) || (yDiff < 0 && swipeMode.value and Mode.BOT.value != 0 && !canScrollUp()))) {
          return false
        }
        if (Math.abs(yDiff) > touchSlop && !isBeingDragged && ((yDiff > 0 && swipeMode.value and Mode.TOP.value != 0) || (yDiff < 0 && swipeMode.value and Mode.BOT.value != 0))) {
          isBeingDragged = true
        }
      }
      MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
        isBeingDragged = false
        activePointerId = -1
      }
      MotionEvent.ACTION_POINTER_UP -> {
        onSecondaryPointerUp(ev)
      }
    }

    return isBeingDragged
  }

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent?): Boolean {
    if (!isBeingDragged || nestedScrollInProgress) {
      return super.onTouchEvent(event)
    }
    when (event?.action) {
      MotionEvent.ACTION_DOWN -> {
        activePointerId = event.getPointerId(event.actionIndex)
      }
      MotionEvent.ACTION_MOVE -> {
        val pointerIndex = event.findPointerIndex(activePointerId)
        if (pointerIndex < 0) {
          return false
        }

        val y = event.y
        val yDiff = y - initMotionY
        val scrollDis = yDiff * dragRate
        if ((yDiff > 0 && swipeDirect != BOT) || (yDiff < 0 && swipeDirect != TOP)) {
          moveSpinner(scrollDis)
        } else {
          setTargetOffset(0)
        }
      }
      MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
        if (activePointerId == -1) {
          return false
        }
        val y = event.y
        val overScroll = (y - initMotionY) * dragRate
        finishSpinner(overScroll)
        return false
      }
    }
    return true
  }

  private fun moveSpinner(scrollDis: Float) {
    val isTop = scrollDis > 0
    totalDragDis = if (isTop) topRefreshView.getSwipeMaxDist() else botRefreshView.getSwipeMaxDist()
    val originalDragPercent = scrollDis / totalDragDis
    swipeDirect = if (isTop) TOP else BOT
    val boundedDragPercent = if (originalDragPercent >= 0) Math.min(1f, originalDragPercent) else Math.max(-1f, originalDragPercent)
    val extraOS = Math.abs(scrollDis) - totalDragDis
    val slingShotDist = totalDragDis
    val tensionSlingshotPercent = Math.max(0f, extraOS / slingShotDist)
//    val tensionPercent = (tensionSlingshotPercent / 4 - Math.pow(tensionSlingshotPercent / 4.0, 2.0)) * 1.2f
    val extraMove = slingShotDist * tensionSlingshotPercent / 2
    val targetY = slingShotDist * boundedDragPercent + if (isTop) extraMove else -extraMove
    dragPercent = (targetY / totalDragDis).toFloat()
    setTargetOffset(targetY.toInt() - offsetTop)
    when {
      isTop -> topRefreshView.onSwipe(swipeDirect, dragPercent)
      dragPercent != 0f -> botRefreshView.onSwipe(swipeDirect, Math.abs(dragPercent))
      else -> {
        topRefreshView.onSwipe(swipeDirect, dragPercent)
        botRefreshView.onSwipe(swipeDirect, dragPercent)
      }
    }
  }

  private fun finishSpinner(overScroll: Float) {
    isBeingDragged = false
    if (totalDragDis > 0 && Math.abs(overScroll) > totalDragDis) {
      setRefreshing(true, true)
    } else {
      isRefreshing = false
      animateOffsetToStartPosition()
    }
    swipeDirect = IDLE
    activePointerId = -1
  }

  fun setTargetView(view: View) {
    target?.apply { super.removeView(this) }
    super.addView(view)
    ensureTarget()
  }

  fun setHeadRefreshView(header: View, layoutParams: LayoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)) {
    if (header is RefreshActionView) {
      if (target != null) {
        super.removeViewAt(0)
      }
      header.layoutParams = layoutParams
      topRefreshView = header
      if (target != null) {
        super.addView(header, 0, layoutParams)
      }
    } else {
      Log.e(TAG, "Refresh view must implement RefreshActionView")
    }
  }

  fun setFootRefreshView(footer: View, layoutParams: LayoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)) {
    if (footer is RefreshActionView) {
      if (target != null) {
        super.removeView(botRefreshView as View)
      }
      footer.layoutParams = layoutParams
      botRefreshView = footer
      if (target != null) {
        super.addView(footer)
      }
    } else {
      Log.e(TAG, "Refresh view must implement RefreshActionView")
    }
  }

  fun setProgressColorSchemeColors(inColor: Int, midColor: Int, outColor: Int) {
    if (topRefreshView is DefRefreshView) {
      (topRefreshView as DefRefreshView).setProgressColorSchemeColors(inColor, midColor, outColor)
    }
    if (botRefreshView is DefRefreshView) {
      (botRefreshView as DefRefreshView).setProgressColorSchemeColors(inColor, midColor, outColor)
    }
  }

  fun refreshFinish() {
    if (isRefreshing) {
      setRefreshing(false, false)
    }
  }

  fun setRefreshing() {
    if (!isRefreshing) {
      swipeDirect = SwipeDirect.TOP
      totalDragDis = topRefreshView.getSwipeMaxDist()
      setRefreshing(true, false)
    }
  }

  private fun setRefreshing(isRefreshing: Boolean, notify: Boolean = false) {
    if (this@SwipeRefreshView.isRefreshing != isRefreshing) {
      this@SwipeRefreshView.notify = notify
      ensureTarget()
      this@SwipeRefreshView.isRefreshing = isRefreshing
      if (isRefreshing) {
        animateOffsetToCorrectPosition()
      } else {
        topRefreshView.onRefreshFinish()
        botRefreshView.onRefreshFinish()
        animateOffsetToStartPosition()
      }
    }
  }

  private fun animateOffsetToStartPosition() {
    from = offsetTop
    fromDragPercent = dragPercent
    val animationDuration = Math.abs(offsetAnimationDuration * fromDragPercent)

    animateToStartPosition.apply {
      reset()
      isReturnStart = true
      duration = animationDuration.toLong()
      interpolator = DecelerateInterpolator()
      setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationEnd(p0: Animation?) {
          offsetTop = target?.top ?: 0
          isReturnStart = false
        }

        override fun onAnimationStart(p0: Animation?) {
        }

        override fun onAnimationRepeat(p0: Animation?) {
        }

      })
    }
    clearAnimation()
    startAnimation(animateToStartPosition)
  }

  private fun animateOffsetToCorrectPosition() {
    from = offsetTop
    fromDragPercent = dragPercent

    clearAnimation()
    if (!isRefreshing) {
      animateOffsetToStartPosition()
    } else {
      animateToCorrectPosition.apply {
        reset()
        duration = offsetAnimationDuration.toLong()
        interpolator = DecelerateInterpolator()
        val direct = swipeDirect
        endWithAction {
          if (direct == TOP) {
            topRefreshView.onRefreshStart()
            if (notify) {
              onRefreshListener?.onRefreshTop()
            }
          } else if (direct == BOT) {
            botRefreshView.onRefreshStart()
            if (notify) {
              onRefreshListener?.onRefreshBot()
            }
          }
        }
      }
      startAnimation(animateToCorrectPosition)
    }

    offsetTop = target?.top ?: 0
    target?.setPadding(targetPaddingLeft, targetPaddingTop, targetPaddingRight, targetPaddingBottom)
  }

  private fun moveToStart(interpolatedTime: Float) {
    val targetTop = from - from * interpolatedTime
    val targetPercent = fromDragPercent * (1f - interpolatedTime)
    val offset = targetTop - (target?.top ?: 0)

    dragPercent = targetPercent
    if (fromDragPercent > 0) {
      topRefreshView.onSwipe(TOP, dragPercent)
    } else if (fromDragPercent < 0) {
      botRefreshView.onSwipe(BOT, Math.abs(dragPercent))
    }
    setTargetOffset(offset.toInt())
    target?.setPadding(targetPaddingLeft, targetPaddingTop, targetPaddingRight, targetPaddingBottom)
  }

  private fun onSecondaryPointerUp(ev: MotionEvent) {
    val pointerIndex = ev.actionIndex
    val pointerId = ev.getPointerId(pointerIndex)
    if (pointerId == activePointerId) {
      val newPointerIndex = if (pointerIndex == 0) 1 else 0
      activePointerId = ev.getPointerId(newPointerIndex)
    }
  }

  private fun getMotionEventY(ev: MotionEvent, activePointerId: Int): Float {
    val index = ev.findPointerIndex(activePointerId)
    if (index < 0) {
      return -1f
    }
    return ev.getY(index)
  }

  private fun ensureTarget() {
    if (target != null) {
      return
    }
    target = getChildAt(0)?.apply {
      targetPaddingLeft = this.paddingLeft
      targetPaddingTop = this.paddingTop
      targetPaddingRight = this.paddingRight
      targetPaddingBottom = this.paddingBottom
      super.addView(topRefreshView as View, 0)
      super.addView(botRefreshView as View, 1)
    }
  }

  private fun setTargetOffset(offset: Int) {
    target?.offsetTopAndBottom(offset)
    offsetTop = target?.top ?: 0
  }

  private fun canScroll(): Boolean = swipeMode != Mode.NONE && (canScrollDown() || canScrollUp())

  private fun canScrollUp(): Boolean = !(target?.canScrollVertically(1) ?: false)

  private fun canScrollDown(): Boolean = !(target?.canScrollVertically(-1) ?: false)

  override fun requestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
    if ((android.os.Build.VERSION.SDK_INT < 21 && target is AbsListView) || (target != null && !ViewCompat.isNestedScrollingEnabled(target!!))) {
      // Nope.
    } else {
      super.requestDisallowInterceptTouchEvent(disallowIntercept)
    }
  }

  override fun onStartNestedScroll(child: View, target: View, axes: Int): Boolean =
      isEnabled && !isRefreshing  && !isReturnStart && axes == ViewCompat.SCROLL_AXIS_VERTICAL

  override fun onNestedScrollAccepted(child: View, target: View, axes: Int) {
    nestedScrollingParentHelper.onNestedScrollAccepted(child, target, axes)
    startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL)
    totalUnconsumed = 0f
    nestedScrollInProgress = true
  }

  override fun onStopNestedScroll(target: View) {
    nestedScrollingParentHelper.onStopNestedScroll(target)
    nestedScrollInProgress = false
    if (totalUnconsumed != 0f) {
      finishSpinner(totalUnconsumed * dragRate)
    }
    totalUnconsumed = 0f
    stopNestedScroll()
  }

  override fun onNestedScroll(target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int) {
    this.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, parentOffsetInWindow)
    val dy = dyUnconsumed + parentOffsetInWindow[1]
    totalUnconsumed += dy
    moveSpinner(-totalUnconsumed * dragRate)
  }

  override fun onNestedPreScroll(target: View, dx: Int, dy: Int, consumed: IntArray) {
    if ((dy > 0 && totalUnconsumed < 0f) || (dy < 0 && totalUnconsumed > 0f)) {
      if (Math.abs(dy.toFloat()) > Math.abs(totalUnconsumed)) {
        consumed[1] = dy + totalUnconsumed.toInt()
        totalUnconsumed = 0.0f
      } else {
        totalUnconsumed += dy.toFloat()
        consumed[1] = dy
      }
      moveSpinner(-totalUnconsumed * dragRate)
    }
    val parentConsumed = parentScrollConsumed
    if (this.dispatchNestedPreScroll(dx - consumed[0], dy - consumed[1], parentConsumed, null)) {
      consumed[0] += parentConsumed[0]
      consumed[1] += parentConsumed[1]
    }
  }

  override fun onNestedFling(target: View, velocityX: Float, velocityY: Float, consumed: Boolean): Boolean =
      dispatchNestedFling(velocityX, velocityY, consumed)

  override fun onNestedPreFling(target: View, velocityX: Float, velocityY: Float): Boolean =
      dispatchNestedPreFling(velocityX, velocityY)

  override fun getNestedScrollAxes(): Int = nestedScrollingParentHelper.nestedScrollAxes

  override fun startNestedScroll(axes: Int): Boolean = nestedScrollingChildHelper.startNestedScroll(axes)

  override fun stopNestedScroll() {
    nestedScrollingChildHelper.stopNestedScroll()
  }

  override fun dispatchNestedScroll(dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, offsetInWindow: IntArray?): Boolean =
    nestedScrollingChildHelper.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow)

  override fun dispatchNestedPreScroll(dx: Int, dy: Int, consumed: IntArray?, offsetInWindow: IntArray?): Boolean =
      nestedScrollingChildHelper.dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow)

  override fun dispatchNestedPreFling(velocityX: Float, velocityY: Float): Boolean =
      nestedScrollingChildHelper.dispatchNestedPreFling(velocityX, velocityY)

  override fun dispatchNestedFling(velocityX: Float, velocityY: Float, consumed: Boolean): Boolean =
      nestedScrollingChildHelper.dispatchNestedFling(velocityX, velocityY, consumed)

  override fun setNestedScrollingEnabled(enabled: Boolean) {
    nestedScrollingChildHelper.isNestedScrollingEnabled = enabled
  }

  override fun isNestedScrollingEnabled(): Boolean = nestedScrollingChildHelper.isNestedScrollingEnabled

  override fun hasNestedScrollingParent(): Boolean = nestedScrollingChildHelper.hasNestedScrollingParent()

  fun setOnRefreshListener(onRefreshListener: OnRefreshListener) {
    this@SwipeRefreshView.onRefreshListener = onRefreshListener
  }

  companion object {
    const val TAG = "SwipeRefreshView"
    private const val DEF_DRAG_RATE = 0.35f
    private const val DEF_OFFSET_ANIMATION_DURATION = 500
  }

  enum class SwipeDirect {
    TOP, BOT, IDLE
  }

  enum class Mode(val value: Int) {
    NONE(0),
    TOP(1 shl 0),
    BOT(1 shl 1),
    ALL(1 shl 0 or (1 shl 1));

    companion object {
      fun get(value: Int): Mode {
        return Mode.values().find { it.value == value } ?: Mode.ALL
      }
    }
  }

  private inline fun Animation.endWithAction(crossinline action: () -> Unit) {
    setAnimationListener(object : AnimationListener {
      override fun onAnimationRepeat(p0: Animation?) {}

      override fun onAnimationEnd(p0: Animation?) {
        action()
      }

      override fun onAnimationStart(p0: Animation?) {}
    })
  }
}
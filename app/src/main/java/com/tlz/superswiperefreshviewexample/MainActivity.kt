package com.tlz.superswiperefreshviewexample

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import com.tlz.refresh.OnRefreshListener
import com.tlz.superswiperefreshview.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

//    Thread(Runnable {
//      Thread.sleep(1000)
//      runOnUiThread {
//        super_refresh_view.setRefreshing()
//      }
//    }).start()
    super_refresh_view.setOnRefreshListener(object : OnRefreshListener {
      override fun onRefreshTop() {
        Thread(Runnable {
          Thread.sleep(3000)
          runOnUiThread {
            super_refresh_view.refreshFinish()
          }
        }).start()
      }

      override fun onRefreshBot() {
        Log.d("MainActivity", "start loadmore")
        Thread(Runnable {
          Thread.sleep(3000)
          runOnUiThread {
            super_refresh_view.refreshFinish()
          }
        }).start()
      }
    })
//    btn_start.setOnClickListener { super_refresh_view.setRefreshing() }
//    btn_finish.setOnClickListener { super_refresh_view.refreshFinish() }
    super_refresh_view.setProgressColorSchemeColors(Color.parseColor("#000000"), Color.parseColor("#000000"), Color.parseColor("#000000"))
  }
}
